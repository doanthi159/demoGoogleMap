<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<script src="<?php echo base_url('node_modules'); ?>/jquery/dist/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="<?php echo base_url('node_modules'); ?>/bootstrap/dist/css/bootstrap.css">
	<script src="<?php echo base_url('node_modules'); ?>/bootstrap/dist/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<?php if (isset($map) && count($map) > 0) {
		echo $map['js'];
	} ?>
	<?php if (isset($map_one) && count($map_one) > 0) {
		echo $map_one['js'];
	} ?>
	<?php if (isset($map_two) && count($map_two) > 0) {
		echo $map_two['js'];
	} ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/style.css">

</head>
<div class="container" id="top-menu">
	<div class="row row-menu">
		<div class="col-md-2 col-menu">
			<ul>
				<li><a class="<?php if ($url == ""){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/'); ?>" href="<?php echo base_url('index.php/home/'); ?>">Simple Marker</a></li>
				<li><a class="<?php if ($url == "multiplemarkers"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/multiplemarkers'); ?>">Multiple Markers</a></li>
				<li><a class="<?php if ($url == "polyline"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/polyline'); ?>">Polyline</a></li>
				<li><a class="<?php if ($url == "polygon"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/polygon'); ?>">Polygon</a></li>
				<li><a class="<?php if ($url == "circle"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/circle'); ?>">Circle</a></li>
			</ul>
		</div>
		<div class="col-md-2 col-menu">
			<ul>
				<li><a class="<?php if ($url == "rectangle"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/rectangle'); ?>">Rectangle</a></li>
				<li><a class="<?php if ($url == "groundoverlay"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/groundoverlay'); ?>">Ground Overlay</a></li>
				<li><a class="<?php if ($url == "directions"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/directions'); ?>">Directions</a></li>
				<li><a class="<?php if ($url == "kmllayer"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/kmllayer'); ?>">KML Layer</a></li>
				<li><a class="<?php if ($url == "places"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/places'); ?>">Places API Integration</a></li>
			</ul>
		</div>
		
		<div class="col-md-2 col-menu">
			<ul>
				<li><a class="<?php if ($url == "trafficoverlay"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/trafficoverlay'); ?>">Traffic Overlay</a></li>
				<li><a class="<?php if ($url == "bicyclingoverlay"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/bicyclingoverlay'); ?>">Bicycling Overlay</a></li>
				<li><a class="<?php if ($url == "panoramio"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/panoramio'); ?>">Panoramio Layer</a></li>
				<li><a class="<?php if ($url == "placesautocomplete"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/placesautocomplete'); ?>">Places Input Autocomplete</a></li>
				
			</ul>
		</div>
		<div class="col-md-2 col-menu">
			<ul>
				<li><a class="<?php if ($url == "cluster"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/cluster'); ?>">Clustering Markers</a></li>
				<li><a class="<?php if ($url == "geolocation"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/geolocation'); ?>">Geolocation</a></li>
				<li><a class="<?php if ($url == "drawingmanager"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/drawingmanager'); ?>">Drawing Manager</a></li>
				<li><a class="<?php if ($url == "styling"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/styling'); ?>">Styling The Map</a></li>
				<li><a class="<?php if ($url == "streetview"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/streetview'); ?>">Street View</a></li>
			</ul>
		</div>
		<div class="col-md-4 col-menu">
			<ul>
				<li><a class="<?php if ($url == "mapclickaddmarker"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/mapclickaddmarker'); ?>">Add Marker At Clicked Position</a></li>
				<li><a class="<?php if ($url == "dragmarker"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/dragmarker'); ?>">Get Co-ordinates Of Dragged Marker</a></li>
				<li><a class="<?php if ($url == "updatedatabase"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/updatedatabase'); ?>">Pass Co-ordinates Back To The Database</a></li>
				<li><a class="<?php if ($url == "multiplemaps"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/multiplemaps'); ?>">Multiple Maps On The Same Page</a></li>
				<li><a class="<?php if ($url == "mapclick"){ echo 'active';} ?>" href="<?php echo base_url('index.php/home/mapclick'); ?>" >Get Co-ordinates At Clicked Position</a></li>
			</ul>
		</div>
		
	</div>
</div>
<div class="container" id="top-menu">
	<div class="row">
	<div class="col-md-12">
		<?php if (isset($map) && count($map) > 0) {
			echo $map['html'];
		} ?>
		<?php if (isset($map_one) && count($map_one) > 0) {
			echo $map_one['html'];
		} ?>
		<?php if (isset($map_two) && count($map_two) > 0) {
			echo $map_two['html'];
		} ?>
		<div id="directionsDiv"></div>
	</div>
	</div>
</div>
	
</body>
</html>