<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {

		parent::__construct();
	    
	    $this->load->library('googlemaps');

	}

	public function index() {	
		
		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';
		
		$this->googlemaps->initialize($config);

	    $marker = array();

		$marker['position'] = '37.429, -122.1419';
		
		$this->googlemaps->add_marker($marker);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = '';
		
		$this->load->view('Home/index', $data);
	}

	public function geolocation() {
		
		$config = array();
		
		$config['center'] = 'auto';
		
		$config['onboundschanged'] = "if (!centreGot) {
			var mapCentre = map.getCenter();
			marker_0.setOptions({
				position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
			});
		} centreGot = true;";
		
		$marker = array();

		$this->googlemaps->initialize($config);
		
		$this->googlemaps->add_marker($marker);

		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'geolocation';
		
		$this->load->view('Home/index', $data);

	}

	public function multiplemarkers() {

		$config['center'] = '37.4419, -122.1419';
		
		$config['zoom'] = 'auto';
		
		$this->googlemaps->initialize($config);

		$marker = array();
		
		$marker['position'] = '37.429, -122.1519';
		
		$marker['infowindow_content'] = '1 - Hello World!';
		
		$marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
		
		$this->googlemaps->add_marker($marker);

		$marker = array();
		
		$marker['position'] = '37.409, -122.1319';
		
		$marker['draggable'] = TRUE;
		
		$marker['animation'] = 'DROP';
		
		$this->googlemaps->add_marker($marker);

		$marker = array();
		
		$marker['position'] = '37.449, -122.1419';
		
		$marker['onclick'] = 'alert("You just clicked me!!")';
		
		$this->googlemaps->add_marker($marker);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'multiplemarkers';
		
		$this->load->view('Home/index', $data);

	}
	public function polyline() {
		
		$config['center'] = '37.4419, -122.1419';
		
		$config['zoom'] = 'auto';
		
		$this->googlemaps->initialize($config);

		$polyline = array();
		
		$polyline['points'] = array('37.429, -122.1319',
					    '37.429, -122.1419',
					    '37.4419, -122.1219');
		
		$this->googlemaps->add_polyline($polyline);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'polyline';
		
		$this->load->view('Home/index', $data);

	}
	public function polygon() {

		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';
		
		$this->googlemaps->initialize($config);

		$polygon = array();
		
		$polygon['points'] = array('37.425, -122.1321',
					   '37.4422, -122.1622',
					   '37.4412, -122.1322',
					   '37.425, -122.1021');
		
		$polygon['strokeColor'] = '#000099';
		
		$polygon['fillColor'] = '#000099';
		
		$this->googlemaps->add_polygon($polygon);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'polygon';
		
		$this->load->view('Home/index', $data);
	
	}
	public function circle() {

		$config['center'] = '37.4419, -122.1419';
		
		$config['zoom'] = 17;
		
		$this->googlemaps->initialize($config);

		$circle = array();
		
		$circle['center'] = '37.4419, -122.1419';
		
		$circle['radius'] = '100';
		
		$this->googlemaps->add_circle($circle);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'circle';
		
		$this->load->view('Home/index', $data);

	}

	public function rectangle() {

		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';
		
		$this->googlemaps->initialize($config);

		$rectangle = array();
		
		$rectangle['positionSW'] = '40.712216,-74.22655';
		
		$rectangle['positionNE'] = '40.773941,-74.12544';
		
		$this->googlemaps->add_rectangle($rectangle);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'rectangle';
		
		$this->load->view('Home/index', $data);

	}
	public function groundoverlay() {

		$config['center'] = '37.4419, -122.1419';
		
		$config['zoom'] = 'auto';
		
		$this->googlemaps->initialize($config);

		$groundOverlay = array();
		
		$groundOverlay['image'] = 'http://maps.google.com/intl/en_ALL/images/logos/maps_logo.gif';
		
		$groundOverlay['positionSW'] = '40.712216,-74.22655';
		
		$groundOverlay['positionNE'] = '40.773941,-74.12544';
		
		$this->googlemaps->add_ground_overlay($groundOverlay);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'groundoverlay';
		
		$this->load->view('Home/index', $data);

	}
	public function directions() {

		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';
		
		$config['directions'] = TRUE;
		
		$config['directionsStart'] = 'empire state building';
		
		$config['directionsEnd'] = 'statue of liberty';
		
		$config['directionsDivID'] = 'directionsDiv';
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'directions';
		
		$this->load->view('Home/index', $data);

	}
	public function cluster() {

		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';
		
		$config['cluster'] = TRUE;
		
		$this->googlemaps->initialize($config);

		$marker = array();
		
		$marker['position'] = '37.429, -122.1419';
		
		$this->googlemaps->add_marker($marker);

		$marker = array();
		
		$marker['position'] = '32.429, -112.1419';
		
		$this->googlemaps->add_marker($marker);

		$marker = array();
		
		$marker['position'] = '35.429, -120.1419';
		
		$this->googlemaps->add_marker($marker);

		$marker = array();
		
		$marker['position'] = '54.429, -0.1419';
		
		$this->googlemaps->add_marker($marker);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'cluster';
		
		$this->load->view('Home/index', $data);

	}
	public function bicyclingoverlay() {

		$config['bicyclingOverlay'] = TRUE;
		
		$this->googlemaps->initialize($config);

		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'bicyclingoverlay';
		
		$this->load->view('Home/index', $data);

	}
	public function kmllayer() {

		$config['kmlLayerURL'] = 'http://api.flickr.com/services/feeds/geo/?g=322338@N20&lang=en-us&format=feed-georss';
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'kmllayer';
		
		$this->load->view('Home/index', $data);

	}
	public function places() {

		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';
		
		$config['places'] = TRUE;
		
		$config['placesLocation'] = '37.4419, -122.1419';
		
		$config['placesRadius'] = 200; 
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'places';
		
		$this->load->view('Home/index', $data);

	}
	public function trafficoverlay() {

		$config['trafficOverlay'] = TRUE;
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'trafficoverlay';
		
		$this->load->view('Home/index', $data);

	}

	public function panoramio() {

		$config['zoom'] = '7';
		
		$config['panoramio'] = TRUE;
		
		$config['panoramioTag'] = 'sunset';
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'panoramio';
		
		$this->load->view('Home/index', $data);

	}
	public function placesautocomplete() {
		

		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';

		$config['places'] = TRUE;

		$config['placesAutocompleteInputID'] = 'myPlaceTextBox';

		$config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport

		$config['placesAutocompleteOnChange'] = 'alert(\'You selected a place\');';

		$this->googlemaps->initialize($config);

		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'placesautocomplete';
		
		$this->load->view('Home/index', $data);

	}
	public function drawingmanager() {
		
		$config['center'] = 'Adelaide, Australia';
		
		$config['zoom'] = '13';
		
		$config['drawing'] = true;
		
		$config['drawingDefaultMode'] = 'circle';
		
		$config['drawingModes'] = array('circle','rectangle','polygon');
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'drawingmanager';
		
		$this->load->view('Home/index', $data);

	}
	public function styling() {
		
		$config['center'] = '1600 Amphitheatre Parkway in Mountain View, Santa Clara County, California';
		
		$config['zoom'] = '13';
		
		$config['styles'] = array(
		 	array("name"=>"Red Parks", "definition"=>array(
		    array("featureType"=>"all", "stylers"=>array(array("saturation"=>"-30"))),
		    array("featureType"=>"poi.park", "stylers"=>array(array("saturation"=>"10"), array("hue"=>"#990000")))
		  )),
		  	array("name"=>"Black Roads", "definition"=>array(
		    array("featureType"=>"all", "stylers"=>array(array("saturation"=>"-70"))),
		    array("featureType"=>"road.arterial", "elementType"=>"geometry", "stylers"=>array(array("hue"=>"#000000")))
		  )),
		  	array("name"=>"No Businesses", "definition"=>array(
		    array("featureType"=>"poi.business", "elementType"=>"labels", "stylers"=>array(array("visibility"=>"off")))
		  ))
		);

		$config['stylesAsMapTypes'] = true;
		
		$config['stylesAsMapTypesDefault'] = "Black Roads"; 
		
		$this->googlemaps->initialize($config);

		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'styling';
		
		$this->load->view('Home/index', $data);

	}
	public function streetview() {
		
		$config['center'] = '37.4419, -122.1419';
		
		$config['map_type'] = 'STREET';
		
		$config['streetViewPovHeading'] = 90;
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'streetview';
		
		$this->load->view('Home/index', $data);

	}
	public function mapclickaddmarker() {
		
		$config['center'] = '37.4419, -122.1419';
		
		$config['zoom'] = 'auto';
		
		$config['onclick'] = 'createMarker_map({ map: map, position:event.latLng });';
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'mapclickaddmarker';
		
		$this->load->view('Home/index', $data);

	}
	public function dragmarker() {
		
		$config['center'] = '37.4419, -122.1419';
		
		$config['zoom'] = 'auto';

		$this->googlemaps->initialize($config);

		$marker = array();
		
		$marker['position'] = '37.429, -122.1419';
		
		$marker['draggable'] = true;
		
		$marker['ondragend'] = 'alert(\'You just dropped me at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
		
		$this->googlemaps->add_marker($marker);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'dragmarker';
		
		$this->load->view('Home/index', $data);

	}
	public function updatedatabase() {
		
		$config['center'] = '37.4419, -122.1419';

		$config['zoom'] = 'auto';
		
		$this->googlemaps->initialize($config);

		$marker = array();
		
		$marker['position'] = '37.429, -122.1419';
		
		$marker['draggable'] = true;
		
		$marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
		
		$this->googlemaps->add_marker($marker);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'updatedatabase';
		
		$this->load->view('Home/index', $data);

	}
	public function multiplemaps() {
		
				// Map One
		$config['center'] = '37.4419, -122.1419';
		$config['zoom'] = 9;
		$config['map_name'] = 'map_one';
		$config['map_div_id'] = 'map_canvas_one';
		$this->googlemaps->initialize($config);

		$marker = array();
		$marker['position'] = '37.429, -122.1419';
		$marker['infowindow_content'] = "I'm on Map One";
		$this->googlemaps->add_marker($marker);

		$data['map_one'] = $this->googlemaps->create_map();

		// Map Two
		$config['center'] = '39.1419, -123.0419';
		$config['zoom'] = 9;
		$config['map_name'] = 'map_two';
		$config['map_div_id'] = 'map_canvas_two';
		$this->googlemaps->initialize($config);

		$marker = array();
		$marker['position'] = '39.1419, -123.0419';
		$marker['infowindow_content'] = "I'm on Map Two";
		$this->googlemaps->add_marker($marker);

		$data['map_two'] = $this->googlemaps->create_map();

		$data['url'] = 'multiplemaps';
		
		$this->load->view('Home/index', $data);

	}
	public function mapclick() {
		
		$config['center'] = '37.4419, -122.1419';
		
		$config['zoom'] = 'auto';
		
		$config['onclick'] = 'alert(\'You just clicked at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
		
		$this->googlemaps->initialize($config);
		
		$data['map'] = $this->googlemaps->create_map();

		$data['url'] = 'mapclick';
	
		
		$this->load->view('Home/index', $data);

	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */